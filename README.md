<a href="https://github.com/rokhimin/ruby-CRUD-postgreSql"><img src="https://media1.tenor.com/images/6e23cd74106fc9ff4fbc4540ba516426/tenor.gif?itemid=5321438" width="250" align="right"/></a>
# Ruby CRUD PostgreSql
A ruby CRUD(Create, Read, Update, Delete) using postgreSql database

## Info
I use active record to control postgreSql like ruby on rails .
but it's without RoR :v very simple

## Requires
1. Ruby
2. PostgreSQL DB 

## Setup
- set configuration, db ,etc 
```config/config.rb```
- install dependencies
```bundle install```
- create table db
```rake db:migrate```

## Run
```ruby linkstart.rb```

## Structure table
###### Magi => ::chara::,::rate::,::description:: (id,created_at,updated_at . autoGenerate)
![](https://i.imgur.com/Dyrum2l.jpg)

## License
MIT License.



